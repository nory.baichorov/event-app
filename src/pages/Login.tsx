import React, { FC } from 'react';
import { Card, Layout, Row } from "antd";
import '../App.css';
import LoginForm from "../components/LoginForm";

const Login: FC = (): JSX.Element => {
    return (
        <Layout>
            <Row justify='center' align='middle' className='h100'>
                <Card className='loginWrapper'>
                    <LoginForm />
                </Card>
            </Row>
        </Layout>
    );
};

export default Login;