import React, { useEffect } from 'react';
import AppRouter from "./components/AppRouter";
import Header from "./components/Header";
import { Layout } from "antd";
import './App.css';
import { useActions } from "./hooks/useActions";
import { IUser } from "./models/IUser";

const App = () => {
    const {setUser, setAuth} = useActions();

    useEffect(() => {
        if (localStorage.getItem('username')) {
            setUser({username: localStorage.getItem('username' || '')} as IUser);
            setAuth(true);
        }
    }, []);

  return (
    <Layout>
        <Header />
        <Layout.Content>
            <AppRouter />
        </Layout.Content>
    </Layout>
  );
}

export default App;
