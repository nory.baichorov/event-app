import React, { FC, useState } from 'react';
import { Button, DatePicker, Form, Input, Row, Select } from "antd";
import { IUser } from "../models/IUser";
import { IEvent } from "../models/IEvent";
import { Moment } from "moment";
import { formatDate } from "../utils/date";
import {useTypedSelector} from "../hooks/useTypedSelector";

interface EventFormProps {
    guests: IUser[];
    submit: (event: IEvent) => void;
}

const EventForm: FC<EventFormProps> = (props): JSX.Element => {
    const [event, setEvent] = useState({
        author: '',
        guest: '',
        date: '',
        description: ''
    } as IEvent);

    const {user} = useTypedSelector(state => state.auth)

    const selectDate = (date: Moment | null) => {
        if (date) {
            setEvent({...event, date: formatDate(date.toDate())})
        }
    };

    const formSubmit = () => {
        props.submit({...event, author: user.username})
    };

    return (
        <Form onFinish={formSubmit}>
            <Form.Item
                label='Описание события'
                name='description'
                rules={[{required: true, message: 'Обязательное поле'}]}>
                <Input
                    value={event.description}
                    onChange={e => setEvent({...event, description: e.target.value })}/>
            </Form.Item>
            <Form.Item
                label='Дата события'
                name='date'
                rules={[{required: true, message: 'Обязательное поле'}]}>
                <DatePicker onChange={(date) => selectDate(date)} />
            </Form.Item>
            <Form.Item
                label='Выберите гостя'
                name='guest'
                rules={[{required: true, message: 'Обязательное поле'}]}>
                <Select onChange={(guest: string) => setEvent({...event, guest})}>
                    {props.guests.map(guest =>
                        <Select.Option
                            key={guest.username}
                            value={guest.username}>
                            {guest.username}
                        </Select.Option>
                    )}
                </Select>
            </Form.Item>
            <Row justify='end'>
                <Form.Item>
                    <Button type='ghost' htmlType='submit'>
                        Добавить
                    </Button>
                </Form.Item>
            </Row>
        </Form>
    );
};

export default EventForm;