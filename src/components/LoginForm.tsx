import React, { FC, useState } from 'react';
import { Button, Form, Input } from "antd";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { useActions } from "../hooks/useActions";

const LoginForm: FC = (): JSX.Element => {
    const {error, isLoading} = useTypedSelector(state => state.auth);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const {login} = useActions();

    const onSubmit = () => {
        login(username, password);
    }
    return (
        <Form onFinish={onSubmit}>
            {error && <div style={{color: 'red'}}>{error}</div>}
            <Form.Item label='Имя пользователя'
                       name='username'
                       rules={[{ required: true, message: 'Обязательное поле!' }]}>
                <Input
                    value={username}
                    onChange={e => setUsername(e.target.value)} />
            </Form.Item>
            <Form.Item label='Пароль'
                       name='password'
                       rules={[{ required: true, message: 'Обязательное поле!' }]}>
                <Input
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    type='password'/>
            </Form.Item>
            <Form.Item>
                <Button type='ghost' htmlType='submit' loading={isLoading}>
                    Войти
                </Button>
            </Form.Item>
        </Form>
    );
};

export default LoginForm;