import React, { FC } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import { authRoutes, publicRoutes, RouteNames } from "../routes";
import { useTypedSelector } from "../hooks/useTypedSelector";

const AppRouter: FC = (): JSX.Element => {
    const { isAuth } = useTypedSelector(state => state.auth)

    return (
        isAuth ?
            <Switch>
                {authRoutes.map(route =>
                    <Route key={route.path}
                           path={route.path}
                           component={route.component}
                           exact={route.exact} />
                )}
                <Redirect to={RouteNames.EVENT} />
            </Switch>
            : <Switch>
                {publicRoutes.map(route =>
                    <Route key={route.path}
                           path={route.path}
                           component={route.component}
                           exact={route.exact}/>
                )}
            <Redirect to={RouteNames.LOGIN} />
            </Switch>
    );
};

export default AppRouter;
